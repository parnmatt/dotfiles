-- gruvbox colours (@morhetz [GitHub]) */

local lexers = vis.lexers

local colors = {
	rd          = '#cc241d',
	green        = '#98971a',
	yellow       = '#d79921',
	blue         = '#458588',
	purple       = '#b16286',
	aqua         = '#689d6a',
	gray         = '#a89984',

	fg           = '#ebdbb2',
	fg0          = '#fbf1c7',
	fg1          = '#ebdbb2',
	fg2          = '#d5c4a1',
	fg3          = '#bdae93',
	fg4          = '#a89984',

	bg           = '#282828',
	bg0          = '#282828',
	bg0_h        = '#1d2021',
	bg0_s        = '#32302f',
	bg1          = '#3c3836',
	bg2          = '#504945',
	bg3          = '#665c54',
	bg4          = '#7c6f64',

	brightgray   = '#928374',
	brightred    = '#fb4934',
	brightgreen  = '#b8bb26',
	brightyellow = '#fabd2f',
	brightblue   = '#83a598',
	brightpurple = '#d3869b',
	brightaqua   = '#8ec07c',
}

lexers.colors = colors