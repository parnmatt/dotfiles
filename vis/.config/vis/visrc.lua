require('vis')

backup = require('plugins/vis-backup/backup')
backup.get_fname = function(_, filepath)
	return filepath .. '~'
end

require('plugins/vis-commentary/vis-commentary')

cursors = require('plugins/vis-cursors/cursors')
cursors.path = os.getenv('XDG_CACHE_HOME') .. '/vis/cursors'
os.execute('mkdir -p $(dirname ' .. cursors.path .. ')')

require('plugins/vis-pairs/pairs')
require('plugins/vis-surround/surround')

vis.events.subscribe(vis.events.INIT, function()
	-- global configuration options
	vis:command('set theme dark-16')
end)

vis.events.subscribe(vis.events.WIN_OPEN, function(win)
	-- per window configuration options
	vis:command('set number')
	vis:command('set relativenumber')
	vis:command('set tabwidth 4')
	vis:command('set autoindent')

	vis:command('map insert jk <Escape>')
end)
