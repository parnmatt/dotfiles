require("vis")
local vis = vis

local M

local builtin_textobjects = {
	["["] = {{ "[" , "]" }, id =  7},  -- +/VIS_TEXTOBJECT_OUTER_SQUARE_BRACKET vis.h
	["{"] = {{ "{" , "}" }, id =  9},  -- +/VIS_TEXTOBJECT_OUTER_CURLY_BRACKET vis.h
	["<"] = {{ "<" , ">" }, id = 11},  -- +/VIS_TEXTOBJECT_OUTER_ANGLE_BRACKET vis.h
	["("] = {{ "(" , ")" }, id = 13},  -- +/VIS_TEXTOBJECT_OUTER_PARENTHESIS vis.h
	['"'] = {{ '"' , '"' }, id = 15},  -- +/VIS_TEXTOBJECT_OUTER_QUOTE vis.h
	["'"] = {{ "'" , "'" }, id = 17},  -- +/VIS_TEXTOBJECT_OUTER_SINGLE_QUOTE vis.h
	["`"] = {{ "`" , "`" }, id = 19},  -- +/VIS_TEXTOBJECT_OUTER_BACKTICK vis.h
	        {{ ""  , ""  }, id = 28},  -- +/VIS_TEXTOBJECT_INVALID vis.h
}

local aliases = {}
for key, data in pairs(builtin_textobjects) do pair = data[1] aliases[pair[2]] = key ~= pair[2] and data or nil end
for alias, data in pairs(aliases) do builtin_textobjects[alias] = data end
for alias, key in pairs({
	B = "{",
	b = "(",
}) do builtin_textobjects[alias] = builtin_textobjects[key] end

local function get_pair(key, pos) return builtin_textobjects[key] and builtin_textobjects[key][1] end

local function add(file, range, pos)
	local d = get_pair(M.key[1], pos)
	-- XXX: How do I tell apart non-existing textobjects from empty ones, like inner <>
	if not (d and range.finish > range.start) then return pos end
	file:insert(range.finish, d[2])
	file:insert(range.start, d[1])
	return pos + #d[1]
end

local function change(file, range, pos)
	local o = get_pair(M.key[1], pos)
	local n = get_pair(M.key[2], pos)
	if not (o and n
		and range.finish > range.start
		and file:content(range.start, #o[1]):find(o[1], 1, true)
		and file:content(range.finish - #o[2], #o[2]):find(o[2], 1, true)) then
		return pos
	end
	file:delete(range.finish - #o[2], #o[2])
	file:insert(range.finish - #o[2], n[2])
	file:delete(range.start, #o[1])
	file:insert(range.start, n[1])
	return pos - #o[1] + #n[1]
end

local function delete(file, range, pos)
	local d = get_pair(M.key[1], pos)
	if not (d
		and range.finish > range.start
		and file:content(range.start, #d[1]):find(d[1], 1, true)
		and file:content(range.finish - #d[2], #d[2]):find(d[2], 1, true)) then
		return pos
	end
	file:delete(range.finish - #d[2], #d[2])
	file:delete(range.start, #d[1])
	return pos - #d[1]
end

local function outer(key) return builtin_textobjects[key] and builtin_textobjects[key].id or builtin_textobjects[1].id end

local function va_call(id, nargs, needs_range)
	return function(keys)
		if #keys < nargs then return -1 end
		if #keys == nargs then
			M.key = {}
			for key in keys:gmatch(".") do table.insert(M.key, key) end
			vis:operator(id)
			if needs_range then
				vis:textobject(outer(M.key[1]))
			end
		end
		return #keys
	end
end

local function operator_new(prefix, handler, nargs, help)
	local id = vis:operator_register(handler)
	if id < 0 then
		return false
	end
	if prefix then
		local needs_range = ({[change] = true, [delete] = true})[handler]
		vis:map(vis.modes.NORMAL, prefix, va_call(id, nargs, needs_range), help)
		vis:map(vis.modes.VISUAL, prefix, va_call(id, nargs), help)
	end
	return id
end

vis.events.subscribe(vis.events.INIT, function()
	M.operator = {
		add = operator_new(M.prefix.add, add, 1, "Add delimiters at range boundaries"),
		change = operator_new(M.prefix.change, change, 2, "Change delimiters at range boundaries"),
		delete = operator_new(M.prefix.delete, delete, 1, "Delete delimiters at range boundaries"),
	}
	if package.loaded["pairs"] then
		local p = require("pairs")
		get_pair = p.get_pair
		outer = function(key) p.key = key return p.textobject.outer end
	end
end)

M = {
	prefix = {add = "sa", change = "sc", delete = "sd"},
}

return M
