# vis-cursors
Remember cursor position per file, for the [vis editor](https://github.com/martanne/vis).

# Usage
Load `cursors.lua` in your `visrc.lua` file ( see [Plugins](https://github.com/martanne/vis/wiki/Plugins) ).

Cursor positions are stored in `$HOME/.cursors`, or you can set the `module.path` variable.
